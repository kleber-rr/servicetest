import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComentarioComponent } from './comentario/comentario.component';
import { HttpClientModule } from '@angular/common/http';
import { ComentarioServiceService } from './comentario/comentario-service.service';

@NgModule({
  declarations: [
    AppComponent,
    ComentarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ComentarioServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
