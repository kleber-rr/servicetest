import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comentario } from './comentario.modelo';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ComentarioServiceService {

  private url = 'http://jsonplaceholder.typicode.com/comments';

  constructor(private http: HttpClient) { }

  getComments(): Observable<Comentario[]> {
    return this.http.get<Comentario[]>(this.url).pipe(
      map(res => res || []),
      catchError(error => this.handleError(error.message || error))
    );
  }

  handleError(handleError: any): import('rxjs').OperatorFunction<any, any> {
    throw new Error('Method not implemented.');
  }

}
