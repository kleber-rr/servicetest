import { Component, OnInit } from '@angular/core';
import { ComentarioServiceService } from './comentario-service.service';
import { Comentario } from './comentario.modelo';

@Component({
  selector: 'app-comentario',
  templateUrl: './comentario.component.html',
  styleUrls: ['./comentario.component.css']
})
export class ComentarioComponent implements OnInit {

  comments: Comentario[];

  constructor(private commentService: ComentarioServiceService) { }

  ngOnInit() {
    this.commentService.getComments()
          .subscribe(com => this.comments = com);
  }

}
